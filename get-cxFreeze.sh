#!/bin/bash

wget "https://pypi.python.org/packages/source/c/cx_Freeze/cx_Freeze-4.3.4.tar.gz"
tar -xzf cx_Freeze-4.3.4.tar.gz
cd cx_Freeze-4.3.4
sed -i 's/if not vars.get(\"Py_ENABLE_SHARED\", 0):/if True:/g' setup.py
python setup.py install
cd ..

