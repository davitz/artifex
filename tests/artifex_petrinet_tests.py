import artifex_petrinet as ap
import unittest
import pickle

class TestNet(unittest.TestCase):
	def get(self, label):
		components = self.petrinet.transitions
		try:
			return next(x for x in self.petrinet.places |\
					self.petrinet.transitions | \
					self.petrinet.arcs \
					if x.label == label)
		except StopIteration:
			return None

	def setUp(self):
		with open('tests/test.artifex', 'rb') as fh:
			self.petrinet = pickle.load(fh) 

	'''
		Test to check the addition of an arc object
		to the petrinet. Test covers the addition of
		that connects two nodes of the same type and
		the proper addition of arcs different node
		types.
	'''
	def test_add_arc(self):
		self.petrinet.add(ap.PetriArc('a0', self.get('p1'), self.get('p2')))
		self.assertIsNone(self.get('a0'))

		self.petrinet.add(ap.PetriArc('a0', self.get('p3'), self.get('t2')))
		self.assertIsNotNone(self.get('a0'))

	'''
		Test to check the removal of a node object
		from the petrinet. Test covers the removal
		of a node connected to and by arcs.
	'''
	def test_remove_node(self):
		self.assertIsNotNone(self.get('p2'))
		self.assertNotEquals(self.petrinet.arcsFrom(self.get('p2')), set())
		self.assertNotEquals(self.petrinet.arcsTo(self.get('p2')), set())

		self.petrinet.remove(self.get('p2'))

		self.assertEquals(self.petrinet.arcsFrom(self.get('p2')), set())
		self.assertEquals(self.petrinet.arcsTo(self.get('p2')), set())
		self.assertIsNone(self.get('p2'))

	'''
		Test to check the simulation of a petrinet.
		Test checks expected values for places before
		and after simulation. The test also covers the
		which transitions can fire before and after
		the simulation.
	'''
	def test_simulation(self):
		# pre conditions
		self.assertEqual(self.get('p1').tokens, 1)
		self.assertEqual(self.get('p2').tokens, 0)

		self.assertTrue(self.petrinet.canFire(self.get('t1')))
		self.assertTrue(self.petrinet.canFire(self.get('t2')))
		self.assertFalse(self.petrinet.canFire(self.get('t3')))

		self.petrinet.fireTransition(self.get('t1'))
		self.petrinet.fireTransition(self.get('t2'))

		# post conditions
		self.assertEqual(self.get('p1').tokens, 0)
		self.assertEqual(self.get('p2').tokens, 2)

		self.assertFalse(self.petrinet.canFire(self.get('t1')))
		self.assertTrue(self.petrinet.canFire(self.get('t2')))
		self.assertTrue(self.petrinet.canFire(self.get('t3')))

unittest.main()
