import os
import sys
import pickle
import re
import copy
import time
import random


from PySide.QtCore import *
from PySide.QtGui import *
from PySide.QtUiTools import *
from PySide.QtXml import *

from enum import Enum

from artifex_petrinet import *
import artifex_display as display

class Mode(Enum):
    cursor = 0
    move = 1
    edit = 2
    delete = 3
    add_place = 4
    add_transition = 5
    add_arc = 6    
    pan = 7
    fire_transition = 8

# component to be edited
component = None

class ArtifexController():

    def __init__(self, aMainWindow, anEditWindow, anApp):
        self.mainWindow = aMainWindow
        self.editWindow = anEditWindow
        self.app = anApp
        random.seed()
        self.currentDirectory = "~/"
        self.currentFile = ""
        self.executing = False
        self.restoreLastDirectory()

        self.petrinet = PetriNet()
        self.mode = Mode.pan

        self.initUIelements()

        self.speedAdjust()
        self.disablePlaybackControls()

    def restoreLastDirectory(self):
        try:
            f = open('.lastdir','r')
            self.currentDirectory = f.readline()
            f.close()
        except IOError:
            return

    def updatePath(self,path):
        reg = re.compile(".*/")
        self.currentDirectory = re.findall(reg,path)[0]
        tokens = path.split("/")
        self.currentFile = tokens[len(tokens)-1]
        extended_title = ""
        if self.currentFile != "":
            extended_title = " - "+self.currentFile
        self.mainWindow.setWindowTitle('Artifex'+extended_title)
        try:
            f = open('.lastdir', 'w')
            f.write(self.currentDirectory)
            f.close()
        except IOError:
            print "Couldn't backup directory."
        

    def startApp(self):
        # show main window
        self.mainWindow.show()

        # Enter Qt application main loop
        self.app.exec_()
        sys.exit()

    def setMode(self, aMode):
        self.mode = aMode
        self.displayPane.clearActive()
        self.displayPane.repaint()

    def getMode(self):
        return self.mode

    def getPetrinet(self):
        if(self.executing):
            return self.execNet
        return self.petrinet

    # event shit


    def newProject(self):
        if(self.executing):
            self.stopExecution()
            
        self.petrinet = PetriNet()
        self.currentFile = ""
        self.displayPane.repaint()


    def openProject(self):
        openFileName = QFileDialog.getOpenFileName(self.mainWindow, 'Open Project', self.currentDirectory, selectedFilter = '*.*')
        self.updatePath(openFileName[0])
        with open(openFileName[0], 'rb') as fh:
            self.petrinet = pickle.load(fh)
            self.displayPane.repaint()

	
    def saveProjectAs(self):
        saveFileName = QFileDialog.getSaveFileName(self.mainWindow, 'Save Project As...', self.currentDirectory, selectedFilter = '*.*')
        self.updatePath(saveFileName[0])
        with open(saveFileName[0], 'wb') as fh:
            if self.executing:
                pickle.dump(self.execNet, fh)
            else:
                pickle.dump(self.petrinet, fh)
          
    def saveProject(self):
        if self.currentFile != "":
            with open(self.currentDirectory+self.currentFile, 'wb') as fh:
                pickle.dump(self.petrinet, fh)
        else:
		self.saveProjectAs()
      
    def exitProg(self):
        sys.exit()


    def beginExecution(self):
        self.execNet = copy.deepcopy(self.petrinet)
        self.executing = True

	
	
    # EVENTS FOR NEW EXECUTION BUTTONS
    # STEP: btnStep, stepExecute
    # RUN: btnRun, execute
    # PAUSE: btnPause, pauseExecution
    # STOP: btnStop, stopExecution
    # SPEED: txtSpeed, no events mapped yet

    def isExecuting(self):
        return self.executing

    def stepExecute(self):
        if(not self.executing):
            self.beginExecution()

            
        
        ready = self.execNet.readyTransitions()
        if(len(ready) == 0):
            self.pauseExecution()
            return

        transition = ready[random.randint(0,len(ready)-1)]
        self.displayPane.setActiveObject(transition)
        self.execNet.fireTransition(transition)
        self.displayPane.repaint()
        

    def speedAdjust(self):
        speed = self.speedSlide.value()
        self.stepInterval = 2000 - 2000*(speed/100.0)
        self.timer.setInterval(self.stepInterval)


    def runStep(self):
        self.stepExecute()
    	
    def execute(self):
        self.disableEditing()
        self.enablePlayBackControls()
        self.timer.start(self.stepInterval)
        self.displayPane.clearActive()
        self.mode = Mode.pan
        self.displayPane.repaint()
        
        
    def enablePlayBackControls(self):
        self.btnPause.setEnabled(True)        

    def disablePlaybackControls(self):
        self.btnPause.setEnabled(False)        


    def disableEditing(self):
        self.btnEdit.setEnabled(False)
        self.btnAddTransition.setEnabled(False)
        self.btnAddPlace.setEnabled(False)
        self.btnDelete.setEnabled(False)
        self.btnMove.setEnabled(False)
        self.btnAddArc.setEnabled(False)
        self.btnFire.setEnabled(False)

    def enableEditing(self):
        self.btnEdit.setEnabled(True)
        self.btnAddTransition.setEnabled(True)
        self.btnAddPlace.setEnabled(True)
        self.btnDelete.setEnabled(True)
        self.btnMove.setEnabled(True)
        self.btnAddArc.setEnabled(True)
    	self.btnFire.setEnabled(True)

    def pauseExecution(self):
        self.timer.stop()
        self.disablePlaybackControls()
        self.enableEditing()
        self.displayPane.repaint()
    	
    def stopExecution(self):
        self.timer.stop()
        self.executing = False
        self.disablePlaybackControls()
        self.enableEditing()
        self.displayPane.repaint()
	
    def showEditWindow(self,aComponent):
        global component
        component = aComponent
        name = self.editWindow.findChild(QLineEdit, "txtName")
        tokens = self.editWindow.findChild(QSpinBox, "spinBox_tokens")
        arcWeight = self.editWindow.findChild(QSpinBox, "spinBox_weight")
        lweight = self.editWindow.findChild(QLabel, "label_weight")
        ltokens = self.editWindow.findChild(QLabel, "label_tokens")
        tokens.setValue(0)
        arcWeight.setValue(0)
        self.editWindow.findChild(QLineEdit, "txtName").setText(component.getLabel())

        if(type(component) is PetriPlace):
            tokens.setValue(component.getTokens())
            arcWeight.setReadOnly(True)
            tokens.setReadOnly(False)
            arcWeight.hide()
            tokens.show()
            ltokens.show()
            lweight.hide()

        if(type(component) is PetriArc):
            arcWeight.setValue(component.getWeight())
            arcWeight.setReadOnly(False)
            tokens.setReadOnly(True)
            tokens.hide()
            arcWeight.show()
            ltokens.hide()
            lweight.show()

        if(type(component) is PetriTransition):
            arcWeight.setReadOnly(True)
            tokens.setReadOnly(True)
            tokens.hide()
            arcWeight.hide()
            ltokens.hide()
            lweight.hide()

        self.editWindow.setWindowTitle("Edit Tool - Artifex")
        self.editWindow.show()

    def setEditData(self):
        global component
        name = self.editWindow.findChild(QLineEdit, "txtName").text()
        tokens = self.editWindow.findChild(QSpinBox, "spinBox_tokens").value()
        arcWeight = self.editWindow.findChild(QSpinBox, "spinBox_weight").value()
        if(not name == ""):
            component.setLabel(name)
        if(type(component) is PetriPlace and not tokens == ""):
            component.setTokens(int(tokens))
        if(not type(component) is PetriPlace and not arcWeight == ""):
            component.setWeight(arcWeight)
        self.editWindow.close()
        self.mainWindow.repaint()

    def setArcMode(self):
        self.setMode(Mode.add_arc)

    def setAddTransitionMode(self):
        self.setMode(Mode.add_transition)
                
    def setAddPlaceMode(self):
        self.setMode(Mode.add_place)

    def setDeleteMode(self):
        self.setMode(Mode.delete)

    def setEditMode(self):
        self.setMode(Mode.edit)

    def setMoveMode(self):
        self.setMode(Mode.move)

    def setPanMode(self):
        self.setMode(Mode.pan)

    def setCursorMode(self):
        self.setMode(Mode.cursor)
        
    def setFireTransitionMode(self):
        self.setMode(Mode.fire_transition)

    # initialize UI elements in code and link to an event
    def initUIelements(self):
        # set main window title
        extended_title = ""
        if self.currentFile != "":
            extended_title = " - "+self.currentFile
        self.mainWindow.setWindowTitle('Artifex'+extended_title)

        # get workspace layout
        self.workspace = self.mainWindow.findChild(QLayout, "workspace")

        # instantiate a DisplayPane object
        self.displayPane = display.DisplayPane(self)
        
        # add display pane to workspace
        self.workspace.addWidget(self.displayPane)
        
        mnuNewProject = self.mainWindow.findChild(QAction, "mnuNewProject")
        mnuNewProject.activated.connect(self.newProject)

        mnuOpenProject = self.mainWindow.findChild(QAction, "mnuOpenProject")
        mnuOpenProject.activated.connect(self.openProject)

        mnuSaveProjectAs = self.mainWindow.findChild(QAction, "mnuSaveProjectAs")
        mnuSaveProjectAs.activated.connect(self.saveProjectAs)

        mnuSaveProject = self.mainWindow.findChild(QAction, "mnuSaveProject")
        mnuSaveProject.activated.connect(self.saveProject)

        mnuExit = self.mainWindow.findChild(QAction, "mnuExit")
        mnuExit.triggered.connect(self.exitProg)

        self.btnEdit = self.mainWindow.findChild(QPushButton, "btnEdit")
        self.btnEdit.clicked.connect(self.setEditMode)
	
        self.btnAddTransition = self.mainWindow.findChild(QPushButton, "btnAddTransition")
        self.btnAddTransition.clicked.connect(self.setAddTransitionMode)

        self.btnAddPlace = self.mainWindow.findChild(QPushButton, "btnAddPlace")
        self.btnAddPlace.clicked.connect(self.setAddPlaceMode)

        self.btnDelete = self.mainWindow.findChild(QPushButton, "btnDelete")
        self.btnDelete.clicked.connect(self.setDeleteMode)

        self.btnMove = self.mainWindow.findChild(QPushButton, "btnMove")
        self.btnMove.clicked.connect(self.setMoveMode)

        btnPan = self.mainWindow.findChild(QPushButton, "btnPan")
        btnPan.clicked.connect(self.setPanMode)

        self.btnAddArc = self.mainWindow.findChild(QPushButton, "btnAddArc")
        self.btnAddArc.clicked.connect(self.setArcMode)

	self.btnStep = self.mainWindow.findChild(QPushButton, "btnStep")
	self.btnStep.clicked.connect(self.stepExecute)
	
        self.btnFire = self.mainWindow.findChild(QPushButton, "btnFire")
        self.btnFire.clicked.connect(self.setFireTransitionMode)
        
	btnRun = self.mainWindow.findChild(QPushButton, "btnRun")
	btnRun.clicked.connect(self.execute)
	
	self.btnPause = self.mainWindow.findChild(QPushButton, "btnPause")
	self.btnPause.clicked.connect(self.pauseExecution)
	
	self.btnStop = self.mainWindow.findChild(QPushButton, "btnStop")
	self.btnStop.clicked.connect(self.stopExecution)
	
        self.speedSlide = self.mainWindow.findChild(QSlider, "slider_Speed")
        self.speedSlide.sliderMoved.connect(self.speedAdjust)

        btnOk = self.editWindow.findChild(QPushButton, "btnOK")
        btnOk.clicked.connect(self.setEditData)

        self.timer = QTimer(self.mainWindow)
        self.mainWindow.connect(self.timer, SIGNAL("timeout()"), self.runStep)
