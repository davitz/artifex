
build: setup.py artifex.py artifex_resources.py artifex_petrinet.py artifex_display.py
	python setup.py build

resources: clean_resources artifex_resources.py

artifex_resources.py: artifex_resources.qrc
	pyside-rcc artifex_resources.qrc -o artifex_resources.py

rebuild: clean build

# not working yet. needs root.
install: build
	python setup.py install

clean:
	rm -r build

clean_resources:
	rm artifex_resources.py
