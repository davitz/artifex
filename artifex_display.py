import sys
import time

from numpy.linalg import norm
from numpy import *

from PySide.QtGui import *
from PySide.QtCore import *

# import artifex modules
from artifex_petrinet import *
from artifex_controller import *

char_width = 6


mouseX = 0
mouseY = 0


active_object = None
active_arc = None

# main drawing area for artifex petri nets
class DisplayPane(QWidget):
    

    # positions for drawing different numbers of tokens in a place
    tokenRelativePositions = [(0,0),(10,0),(-10,0),(0,10),(0,-10),(10,10),
                              (-10,10),(10,-10),(-10,-10),(20,0),(-20,0),
                              (0,20),(0,-20)]

    def __init__(self, aController):
        super(DisplayPane, self).__init__()
        self.initUI()
        self.controller = aController
        self.pan_anchor = None
        self.view_offset = array([0,0])
        

    def initUI(self):
        self.setMinimumSize(1, 30)
        sizePolicy = QSizePolicy()
        sizePolicy.setHorizontalPolicy(QSizePolicy.Preferred)
        sizePolicy.setVerticalPolicy(QSizePolicy.Preferred)
        self.setSizePolicy(sizePolicy)

    def setActiveObject(self, new_active):
        global active_object
        active_object = new_active

    def clearActive(self):
        global active_object
        global active_arc
        active_object = active_arc = None

    def offsetMouse(self):
        global mouseX
        global mouseY
        global active_object
        global active_arc
        mouseX -= self.view_offset[0]
        mouseY -= self.view_offset[1]

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing,True)
        self.drawWidget(event, qp)
        qp.end()

    # called when mouse clicked within display pane
    def mousePressEvent(self, QMouseEvent):
        global mouseX
        global mouseY

        def closeEnough():
            if nearest_object != None:
                if abs(nearest_object.getX()-mouseX) < 40 and abs(nearest_object.getY()-mouseY) < 40: return True
            else: return False

        def cursor():
            return

        def move():
            global active_object
            if closeEnough():
                active_object = nearest_object
            elif active_object != None:
                active_object.moveTo(mouseX,mouseY)

        def edit():
            global active_object
            global active_arc

            if closeEnough():                
                active_object = nearest_object
                active_arc = None
                self.repaint()
                self.controller.showEditWindow(nearest_object)

            elif nearest_arc != None and sqrt(self.controller.getPetrinet().arcDist(nearest_arc,mouseX,mouseY)) < 10:
                active_object = None
                active_arc = nearest_arc
                self.repaint()
                self.controller.showEditWindow(nearest_arc)
                
                
        def delete():
            
            if nearest_object != None and closeEnough():
                self.controller.getPetrinet().remove(nearest_object)
            elif nearest_arc != None and self.controller.getPetrinet().arcDist(nearest_arc,mouseX,mouseY) < 10:
                self.controller.getPetrinet().remove(nearest_arc)


        def add_place():
            newPlace = PetriPlace("p" + str(len(self.controller.getPetrinet().getPlaces())+1),mouseX,mouseY, 0)
            self.controller.getPetrinet().add(newPlace)
            

        def add_transition():
            newTrans = PetriTransition("t" + str(len(self.controller.getPetrinet().getTransitions())+1),mouseX,mouseY)
            self.controller.getPetrinet().add(newTrans)
            
            
        def add_arc():
            global active_object

            if active_object == None and closeEnough():
                active_object = nearest_object

        def pan():
            if(self.pan_anchor == None):
                self.pan_anchor = array([mouseX,mouseY])
            else:
                self.view_offset = add(self.view_offset, subtract(array([mouseX,mouseY]), self.pan_anchor))
                self.pan_anchor = array([mouseX, mouseY])
                self.repaint()

        def fire_transition():
            global active_object
            if not self.controller.isExecuting():
                self.controller.beginExecution()
            if closeEnough() and type(nearest_object) is PetriTransition:
                active_object = nearest_object
                if self.controller.getPetrinet().canFire(active_object):
                    self.controller.getPetrinet().fireTransition(active_object)

        actions = { 0 : cursor,
                    1 : move,
                    2 : edit,
                    3 : delete,
                    4 : add_place,
                    5 : add_transition,
                    6 : add_arc,
                    7 : pan,
                    8 : fire_transition
                }
        mouseX = QMouseEvent.x()
        mouseY = QMouseEvent.y()
        if self.controller.getMode() != Mode.pan:
                self.offsetMouse()
        nearest_arc = self.controller.getPetrinet().nearestArc(mouseX,mouseY)
        nearest_object = self.controller.getPetrinet().nearestNode(mouseX,mouseY)
        mode = self.controller.getMode()
        actions[mode]()
        self.repaint()

    # called when mouse released within display pane
    def mouseReleaseEvent(self, QMouseEvent):
        global active_object
        global mouseX
        global mouseY

        
        def closeEnough():
            if abs(nearest_object.getX()-mouseX) < 40 and abs(nearest_object.getY()-mouseY) < 40: return True
            else: return False

        mode = self.controller.getMode()


        if self.controller.getMode() == Mode.add_arc and active_object != None:
            mouseX = QMouseEvent.x()
            mouseY = QMouseEvent.y()
            if mode != Mode.pan:
                self.offsetMouse()
            nearest_object = self.controller.getPetrinet().nearestNode(mouseX,mouseY)
            
            if closeEnough():
                newArc = PetriArc("a"+str(len(self.controller.getPetrinet().getArcs())+1),active_object, nearest_object, 1)
                
                self.controller.getPetrinet().add(newArc)
            

            active_object = None
            self.repaint()

        if mode == Mode.pan:
            self.pan_anchor = None
        
    # called when mouse moved within display pane
    def mouseMoveEvent(self, QMouseEvent):

        global active_object
        global mouseX
        global mouseY

        #checks to make sure mouse is within bounds
        def withinBounds(mouseX, mouseY):
            return True
            if(mouseX < 0): return False
            if(mouseX > self.width()): return False
            if(mouseY < 0): return False
            if(mouseY > self.height()): return False
            return True

        def move():
            if active_object != None and withinBounds(mouseX,mouseY):
                active_object.moveTo(mouseX, mouseY)

        def drawArrow():
            self.repaint()


        mode = self.controller.getMode()
        mouseX = QMouseEvent.x()
        mouseY = QMouseEvent.y()
        if mode != Mode.pan:
            self.offsetMouse()

        if(mode == 1):
            move()
            self.repaint()
        if(mode == 6):
            drawArrow()
        if mode == Mode.pan and self.pan_anchor != None:
            self.view_offset = add(self.view_offset, subtract(array([mouseX,mouseY]), self.pan_anchor))
            self.pan_anchor = array([mouseX, mouseY])
            self.repaint()




    # draw an arrow representing an arc
    # r1 is a margin to leave between origin and start of arrow
    # r2 is a margin to leave between destination and point of arrow
    def drawArrow(self, qp, x1, y1,r1, x2, y2, r2):
        

        arrowScale = 7

        p1 = array([x1, y1])
        p2 = array([x2, y2])
        vec = subtract(p2, p1)
        invse = multiply(-1,vec)
        invse = divide(invse, norm(invse))
        ortho = array([-vec[1],vec[0]])
        ortho = divide(ortho, norm(ortho))
        p2 = add(p2 , multiply(r2,invse))
        p1 = subtract(p1, multiply(r1,invse))
        path = QPainterPath()
        tip = QPoint(p2[0],p2[1])
        path.moveTo(tip)
        corner1 = add(p2 , multiply(arrowScale , add(multiply(2,invse), ortho)))
        path.lineTo(QPoint(corner1[0],corner1[1]))
        corner2 = add(p2 , multiply(arrowScale , subtract(multiply(2,invse), ortho)))
        path.lineTo(QPoint(corner2[0],corner2[1]))
        path.lineTo(tip)

        qp.fillPath(path, QBrush (Qt.black, Qt.SolidPattern))
        p2 = add(p2,multiply(1.5,invse))
        qp.drawLine(p1[0],p1[1],p2[0],p2[1])

                         
    # draw a place
    def drawPlace(self, qp, label, x, y, tokens, isHighlighted):
        
        x += self.view_offset[0]
        y += self.view_offset[1]

        if isHighlighted == True:
            qp.setPen(QPen(Qt.NoPen))
            qp.setBrush(QBrush(Qt.green, Qt.SolidPattern))
            qp.drawEllipse(x-30, y-30,60,60)

        qp.setBrush(QBrush(Qt.white,Qt.SolidPattern))
        qp.setPen(QPen(QBrush(Qt.black), 3, Qt.SolidLine))
        
        qp.drawEllipse(x-25,y-25,50,50)
        
        qp.setPen(QPen(QBrush(Qt.black), 2, Qt.SolidLine))
        qp.setBrush(QBrush(Qt.black, Qt.SolidPattern))

        qp.drawText(x - char_width*(len(label)/2), y-30, label)

        if(tokens < 14):
            for i in range(0,tokens):
                qp.drawEllipse(x + self.tokenRelativePositions[i][0] - 2,
                               y + self.tokenRelativePositions[i][1] - 2,
                               6,
                               6)
        elif(tokens < 100):
            qp.drawText(x - 20, y + 5, tokens.__str__() + "x")
            qp.drawEllipse(x+6,y-3,6,6)
        else:
            qp.drawText(x - 20, y + 5, tokens.__str__() + "x")
            qp.drawEllipse(x+15,y-3,6,6)
            

        qp.setBrush(QBrush(Qt.black, Qt.NoBrush))


    def drawTransition(self, qp, label, x, y, isHighlighted):
        x += self.view_offset[0]
        y += self.view_offset[1]

        if isHighlighted:
            qp.fillRect(x -8, y-23, 16, 46, QBrush(Qt.green, Qt.SolidPattern))

        qp.setBrush(QBrush(Qt.black, Qt.SolidPattern))
        qp.fillRect(x-5,y-20,10,40, Qt.SolidPattern)
        qp.drawText(x - char_width*(len(label)/2), y-25, label)

    
    def drawArc(self, qp, label, weight, sourceNode, targetNode, highlighted):
        transOffset = 0
        
        sx = sourceNode.getX() + self.view_offset[0]
        sy = sourceNode.getY() + self.view_offset[1]

        tx = targetNode.getX() + self.view_offset[0]
        ty = targetNode.getY() + self.view_offset[1]

        # get appropriate delta values

        if(targetNode.getX() > sourceNode.getX()):
            deltaX = targetNode.getX() - sourceNode.getX()
            deltaY = targetNode.getY() - sourceNode.getY()
            textAdjX = 25
        else:
            deltaX = sourceNode.getX() - targetNode.getX()
            deltaY = sourceNode.getY() - targetNode.getY()
            textAdjX = 0
 
        if isinstance(targetNode,PetriTransition) and abs(deltaY) > deltaX:
            if sy < ty:
                transOffset = -15
            else:
                transOffset = 15

       

        # calculate midpoints
        midX = (tx + sx)/2
        midY = (ty + sy)/2

        angle = math.atan2(deltaY,deltaX)

        # correct midpoints for rotation
        newX = midX*cos(-angle) - midY*sin(-angle)
        newY = midY*cos(-angle) + midX*sin(-angle)

        # rotate coordinates to match line
        qp.rotate(degrees(angle))        
        qp.drawText(newX-textAdjX,newY-5,label)
        qp.drawText(newX-textAdjX,newY+15,"weight:"+str(weight))
        qp.rotate(-degrees(angle))

        if isinstance(sourceNode, PetriPlace):
            r1 = 26
            r2 = 3
        else:
            r1 = 3
            r2 = 26
        
        if highlighted:
            qp.setPen(QPen(QBrush(Qt.green),2))

        self.drawArrow(qp, 
                       sx,sy, r1,
                       tx,ty + transOffset, r2)

        qp.setPen(QPen(QBrush(Qt.black), 2))

        

    


    # code to display drawing area
    def drawWidget(self, event, qp):
        global active_object
        global active_arc
        # first, fill area with white
        qp.fillRect(event.rect(), Qt.white)

        # set draw color
        qp.setPen(QPen(Qt.black))
        
        #draw shapes
        
        
        for i in self.controller.getPetrinet().getPlaces():
            self.drawPlace(qp,i.getLabel(), i.getX(), i.getY(), i.getTokens(), i == active_object)

        for i in self.controller.getPetrinet().getTransitions():
            self.drawTransition(qp, i.getLabel(), i.getX(), i.getY(), i == active_object)

        for i in self.controller.getPetrinet().getArcs():
            self.drawArc(qp,i.getLabel(),i.weight, i.getFromNode(), i.getToNode(), 
                         self.controller.getMode() == Mode.edit and i == active_arc)


        if self.controller.getMode() == Mode.add_arc and active_object != None:
            margin = 0
            if isinstance(active_object, PetriPlace):
                margin = 28
            
            self.drawArrow(qp, 
                           active_object.getX() + self.view_offset[0],
                           active_object.getY() + self.view_offset[1],
                           margin, mouseX + self.view_offset[0], mouseY +self.view_offset[1] , 0)
            
