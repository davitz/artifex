from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(
	packages = [], 
	excludes = [],
	include_files = ['ui/','images/']
)

executables = [
    Executable('artifex.py', 'Console')
]

setup(name='Artifex',
      version = '0.2',
      description = '',
      options = dict(build_exe = buildOptions),
      executables = executables,
      requires=['PySide', 'cx_Freeze', 'numpy']
)
