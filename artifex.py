#!/usr/bin/python

# Import PySide classes
import os
import sys
import atexit
import re
import pickle
from PySide.QtCore import *
from PySide.QtGui import *
from PySide.QtUiTools import *
from PySide.QtXml import *

# import artifex modules
from artifex_display import *
from artifex_petrinet import *
from artifex_resources import *
from artifex_controller import *

# Create a Qt application
app = QApplication(sys.argv)

# Load main window
loader = QUiLoader()

if getattr(sys, 'frozen', False):
    # frozen
    uidir = os.path.dirname(sys.executable)+ "/ui/"
    if (re.compile(".*/bin/ui/").match(uidir)):
        m = re.compile("/bin/").search(uidir)
        uidir = uidir[:m.start()] + "/lib/artifex/ui/"
else:
    # unfrozen
    uidir = os.path.dirname(os.path.realpath(__file__))+ "/ui/"


# load template for main window
file = QFile(uidir+"MainWindow.ui")
file.open(QFile.ReadOnly)
mainWindow = loader.load(file)
file.close()

# load template for edit window
file = QFile(uidir+"EditWindow.ui")
file.open(QFile.ReadOnly)
editWindow = loader.load(file)
file.close()


#init the controller
controller = ArtifexController(mainWindow,editWindow,app)



# start application
controller.startApp()

	
	
