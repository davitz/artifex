import math
class PetriComponent(object):

    def __init__(self, label):
        self.label = label
    
    def setLabel(self, label):
        self.label = label

    def getLabel(self):
        return self.label

class PetriNode(PetriComponent):
    def __init__(self, label, x, y):
        PetriComponent.__init__(self, label)
        self.x = x
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def moveTo(self, x, y):
        self.x = x
        self.y = y


class PetriPlace(PetriNode):
    def __init__(self, label, x, y, tokens=1):
        PetriNode.__init__(self, label, x, y)
        self.tokens = tokens

    def setTokens(self, tokens):
        self.tokens = tokens

    def getTokens(self):
        return self.tokens


class PetriTransition(PetriNode):
	def __init__(self, label, x, y):
		PetriNode.__init__(self, label, x, y)

	def setWeight(self, weight):
		self.weight = weight

	def getWeight(self):
		return self.weight
 
class PetriArc(PetriComponent):
	def __init__(self, label, fromNode, toNode, weight=1):
		PetriComponent.__init__(self, label)
		self.fromNode = fromNode
		self.toNode = toNode
		self.weight = weight

	def getFromNode(self):
		return self.fromNode

	def getToNode(self):
		return self.toNode

	def getWeight(self):
		return self.weight

        def setWeight(self, weight):
            self.weight = weight

class PetriNet(object):
	def __init__(self):
		self.places = set()
		self.transitions = set()
		self.arcs = set()

	def getPlaces(self):
		return self.places

	def getTransitions(self):
		return self.transitions

	def getArcs(self):
		return self.arcs

	def nearestNode(self, x, y):
		def dist(item):
			return (item.x - x)**2 + (item.y - y)**2
		try:
			return min(self.places | self.transitions, key=dist)
		except ValueError:
			return None
        def arcDist(self,item,x,y):
            x1, x2 = item.fromNode.x, item.toNode.x
            y1, y2 = item.fromNode.y, item.toNode.y
            vlen = math.sqrt((x2 - x1)**2 + (y2 - y1)**2) 

            px, py = x - x1, y - y1

            dx = px * (x2 - x1) + py * (y2 - y1)
            dy = px * (y1 - y2) + py * (x2 - x1)

            dx /= vlen
            dy /= vlen

            if dx < 0:
                return math.sqrt(dx**2 + dy**2)
            elif dx > vlen: 
                return math.sqrt((dx - vlen)**2 + dy**2) 
            else :
                return math.fabs(dy)

	def nearestArc(self, x, y):
            try:
                return min(self.arcs, key=lambda arc:self.arcDist(arc,x,y))
            except ValueError:
                return None



	def add(self, item):
		if isinstance(item, PetriPlace):
			self.places.add(item)
		if isinstance(item, PetriTransition):
			self.transitions.add(item)
		if isinstance(item, PetriArc):
			if type(item.fromNode) is not type(item.toNode):
				self.arcs.add(item)

	def remove(self, item):
		if isinstance(item, PetriPlace):
			self.places.discard(item)
			map(self.arcs.discard, [x for x in self.arcs if x.toNode is item or x.fromNode is item])
		if isinstance(item, PetriTransition):
			self.transitions.discard(item)
			map(self.arcs.discard, [x for x in self.arcs if x.toNode is item or x.fromNode is item])
		if isinstance(item, PetriArc):
			self.arcs.discard(item)

	def arcsTo(self, node):
            return set(filter(lambda x: x.toNode is node, self.arcs))

	def arcsFrom(self, node):
            return set(filter(lambda x: x.fromNode is node, self.arcs))

	def canFire(self, transition):
            return all(map (lambda x: x.fromNode.tokens >= x.weight, self.arcsTo(transition)))

	def readyTransitions(self):
            return filter(lambda x: isinstance(x, PetriTransition) and self.canFire(x), self.transitions)

	def fireTransition(self, transition):
            for i in self.arcsTo(transition):
                i.fromNode.tokens -= i.weight

            for i in self.arcsFrom(transition):
                i.toNode.tokens += i.weight
